//
//  ViewController.swift
//  ARFaceGeometryLipGap
//
//  Created by Rosemary Carmody on 07/03/2019.
//  Copyright © 2019 John Lewis plc. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class FloatingLipsViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARFaceTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNViewDelegate
    
    
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        var device: MTLDevice!
        device = MTLCreateSystemDefaultDevice()
        
        let faceGeometry = ARSCNFaceGeometry(device: device)
        let node = SCNNode(geometry: faceGeometry)
        
        guard let material = node.geometry?.firstMaterial else { return nil }
        material.diffuse.contents = UIImage(named: "lipShape")
        
        return node
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        
        
        guard let faceAnchor = anchor as? ARFaceAnchor else {return}
        
        
        
        // This dictionary will be used to create a custom ARFaceGeometry based on the default provided by the ARFaceAnchor
        
        var blendShapes: [ARFaceAnchor.BlendShapeLocation: NSNumber] = [:]
        
        
        
        // Looping through each tracked blend shape, replacing values that cross over the arbitrary thresholds
        
        for (blendShape, value) in faceAnchor.blendShapes {
            
            if blendShape == .mouthClose && Float(truncating: value) > 0.7 {
                
                blendShapes[blendShape] = 1
                
            } else if blendShape == .mouthFunnel && Float(truncating: value) < 0.3 {
                
                blendShapes[blendShape] = 0
                
            } else {
                
                blendShapes[blendShape] = value
                
            }
            
        }
        
        
        
        // Creating a new ARFaceGeometry from the adjusted blend shapes
        
        guard let faceGeometry = ARFaceGeometry(blendShapes: blendShapes) else {return}
        
        
        
        // Updating the ARSCNFaceGeometry with the custom ARFaceGeometry
        
        let arscnFaceGeometry = node.geometry as? ARSCNFaceGeometry
        
        arscnFaceGeometry?.update(from: faceGeometry)
        
    }
    
}
